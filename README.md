
# Initial setup

```
./fetch_deps.sh # download dependencies
cp settings.mk.example settings.mk # create initial personal settings file
sudo apt install python3-pip # install pip python package manager
sudo pip3 install esptool # use pip to install esptool
```

# Building firmware

```
make
```

# Flashing firmware

```
make flash
```

# Flashing web app

This assumes that you've already installed the dependencies for the web app. See `web/README.md` for details.

```
./flash_fs.sh
```

# Unit tests

Unit tests are in the `tests/` directory. To use, first ensure you have a recent version of npm installed (I like using [nvm](https://github.com/nvm-sh/nvm) to install npm), then run:

```
cd tests/
npm install
```

Now you ensure you've flashed both firmware and filesystem to your open-spec and that you're connected to its WiFi access point. You can then run any of the tests, e.g:

```
./websocket_fragmented_test.js
```

# ToDo

* With a binary RPC method how are we detecting if an error is returned?

## Complete RPC system
   
* Support read and write streams
* Write API documentation

For streams, the callback should be called every time a chunk of data is available and then the client side can turn it into a proper stream as long as the list of exported functions includes metadata specifying which functions return a stream and which don't.
    
## Unit tests

* Use tape to turn tests into proper unit tests
* Write unit tests for all RPC features
* Update tape to latest version in package.json 

## Get initial setup wizard working

* WiFi connect with password dialog
* Connect to hidden WiFi option
* Show MAC address on WiFi connect page
* Save connection info to flash

## SD card

* Get SD card read/writing working

## CCD

* Talk to the linear CCD
