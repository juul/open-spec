
var me = {
  
  connect: function(t, url, opts, cb) {
    if(typeof opts === 'function') {
      cb = opts;
      opts = {};
    }

    var WebSocketClient = require('websocket').client;

    var client = new WebSocketClient(opts);
    
    client.on('connect', function(connection) {
      t.comment("Connected!");
      cb(null, connection);
    });

    client.on('connectFailed', function(err) {
      cb(err);
    });

    t.comment("Connecting to " + url);
    client.connect(url);
  },

  // if `arg` is a number then create an argument of a length
  // such that the total length of the returned data equals the
  // specified number
  formatRPCall: function(id, functionName, arg) {
    if(id) {
      if(!Buffer.isBuffer(id)) {
        var b = Buffer.alloc(2);
        id = b.writeUInt16LE(id);
      }
    } else {
      id = Buffer.from(['4', '2']); // "unique" ID for RPC call
    }
    if(!Buffer.isBuffer(functionName)) {
      functionName = Buffer.from(functionName); // name of function to call
    }
    var functionNameLength = Buffer.from([functionName.length]);
    if(arg) {
      if(!Buffer.isBuffer(arg)) {
        if(typeof arg === 'number') {
          var testDataSize = arg - id.length - functionNameLength.length - functionName.length;
          arg = '';
          var i;
          for(i=0; i < testDataSize; i++) {
            arg += '.';
          }
        }
        arg = Buffer.from(arg);
      }
      return Buffer.concat([id, functionNameLength, functionName, arg])
    }
    
    return Buffer.concat([id, functionNameLength, functionName]);
  },
  
  parseRPCResponseHeader: function(data) {
    return {
      id: data.slice(0, 2),
      metadata: data.slice(2, 3),
      data: data.slice(3)
    };
  }


};

module.exports = me;
