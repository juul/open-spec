var test = require('tape');

var settings = require('../web/settings.js');
var ws = require('./common/websocket.js');

/*
  Test whether RPC calls that send binary/text
  data arguments work as expected.
*/

test('websocket-binary', function (t) {
  t.plan(1)
  
  ws.connect(t, settings.websocketURL, {
    fragmentOutgoingMessages: false
  }, function(err, connection) {
    if(err) t.fail(err);

    connection.on('message', function(msg) {
      if(msg.type !== 'binary') {
        t.fail("Error: Expected binary data but got data of type: " + msg.type);
      }
      
      var o = ws.parseRPCResponseHeader(msg.binaryData);

      t.assert(o.data.equals(Buffer.from("123 FOO")));
      connection.close();
      t.end();
    });

    var testData = ws.formatRPCall(null, 'test_binary', '123 bar');
    
    t.comment("Sending " + testData.length + " bytes: " + testData);
    connection.send(testData);
  });
});
