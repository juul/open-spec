var test = require('tape');

var settings = require('../web/settings.js');
var ws = require('./common/websocket.js');

/*

  Test whether the websocket server can handle messages 
  that are split into multiple frames and where
  each frame is split into multiple chunks.

  ESPAsyncWebServer will fire multiple WS_EVT_DATA events
  for a single unfragmented message if it is large enough,
  and will also fire multiple WS_EVT_DATA events for a 
  single frame in fragmented messages if that frame is
  larger than some value.
  
  Unfortunately the chunking behavior is undocumented, see:

  `../docs/websocket_fragments_and_chunks.md`
*/

var testDataSize = 4 * 1024; // total bytes sent

test('websocket-binary', function (t) {
  t.plan(3)
  
  ws.connect(t, settings.websocketURL, {
    
    fragmentOutgoingMessages: false
    
  }, function(err, connection) {
    if(err) t.fail(err);

    connection.on('message', function(msg) {
      if(msg.type !== 'binary') {
        t.fail("Error: Expected binary data but got data of type: " + msg.type);
      }
      
      var o = ws.parseRPCResponseHeader(msg.binaryData);

      t.assert(o.data.slice(0, 7).equals(Buffer.from("....FOO")), "checking modified part of returned value");

      var fail = false;
      var i;
      for(i=7; i < o.data.length; i++) {
        if(o.data[i] !== '.') {
          fail = true;
          break;
        }
      }
      t.assert(!fail, "checking remaining data");
      
      t.equals(o.data.length, testDataSize - 14, "checking length of returned value");
      connection.close();
      t.end();
    });

    var testData = ws.formatRPCall(null, 'test_binary', testDataSize);;

    
    t.comment("Sending " + testData.length + " bytes in " + Math.ceil(testData.length / fragmentSize) + " fragments");
    connection.send(testData);
  });
});
