#!/bin/bash

set -e

# Get makeEspArduino.
# We're using a fork until our tiny pull request is accepted
#git clone https://github.com/plerup/makeEspArduino
git clone https://github.com/Juul/makeEspArduino
cd makeEspArduino
git checkout 6874ffb9a64c8725f17e96c00cda0245b2753961
#git checkout tags/4.18.0
cp makeEspArduino.mk ../Makefile
cd ../

# install arduino-esp32 build environment
git clone https://github.com/espressif/arduino-esp32 arduino-esp32
cd arduino-esp32/
git checkout tags/1.0.2
cd tools/
python get.py
cd ../../

# install ESPAsyncTCP and ESPAsyncWebserver
mkdir -p libs
cd libs/
git clone https://github.com/me-no-dev/AsyncTCP
cd AsyncTCP/
git checkout 90715ae6b3ee72e9e40cd3dd2f9609217bf3ee02
cd ../
git clone https://github.com/me-no-dev/ESPAsyncWebServer
cd ESPAsyncWebServer/
git checkout b0c6144886f4e1f88684f708d7b2974143d8601c
cd ../
mkdir -p ArduinoJson
cd ArduinoJson/
wget https://github.com/bblanchon/ArduinoJson/releases/download/v6.11.3/ArduinoJson-v6.11.3.h -O ArduinoJson.h
cd ../

