

The following is based on a combination of reading [section 5.4 of the WebSocket RFC](https://tools.ietf.org/html/rfc6455#section-5.4) and my own testing of the ESPAsyncWebServer async websocket functionality.

The websocket standard allows splitting a single message into multiple frames.

When receiving large unfragmented messages with ESPAsyncWebServer the `WS_EVT_DATA` event will fire multiple times, handing you a chunk of data each time. If you receive a message in multiple fragments then `WS_EVT_DATA` will also fire for each fragment. To make things a bit more interesting these two behaviors combine such that you can receive a message that's effectively split into multiple frames that are then subdivided into multiple chunks. At least chunks never cross frame boundaries. 

The following is based on my testing and may not hold exactly true for all websocket clients. Note that the term "chunk" here is not in any way official terminology. I'm just using it for lack of a better term.

# single-frame un-chunked messages

`info->final` is always `1`
`info->num` may be `0` (but you can probably ignore this value)
`info->index` is `0`
`info->len` is equal to the `len` argument and is the size of the message in bytes.
`info->opcode` is > `0`

# multi-frame messages

For multi-frame messages, all but the last frame will have `info->final == 0` and then for the last frame `info->final == 1`. All frames in the same multi-frame message will have the same `info->num` so you can think of this as the message number (however `info->num` seems to always be 0 for single-frame messages). For each frame, `info->len` will be the length of the frame. Unfortunately `info->index` is `0` for all frames in a message so it seems like the only way to know the ordering of received frames is by the order in which they arrive. This should be fine since websockets are only used over TCP (as far as I know).

# chunking

If a message or a frame is chunked then `info->index` will be `0` for the first chunk and then for each subsequent chunk it will be the starting index in bytes for that chunk. E.g. if you get a 200-byte frame or mesage in two chunks of 100 bytes then the first chunk will have `info->index == 0` and the second `info->index == 100`.

The `len` argument will be the length in bytes of the current chunk. So if you see that `info->len == len` then you know that this is a single-chunk message.
