import {h, render, Component} from 'preact';
import Socket from './socket.js';

import Root from './components/Root.js';

var app = {
  state: 'INIT' // first state. system initializing
};

window.app = app;

function renderAll() {
  var container = document.getElementById('container');
  
  render((
    <Root />
  ), container);
}


function init() {
  app.socket = new Socket('/ws', {debug: true});

  app.actions = require('./actions/index');
  
  renderAll();
}

init();
