

import { h, render, Component } from 'preact';
import Router from 'preact-router';
import { route } from 'preact-router';
import Akima from 'akima-interpolator';

var app;
// Scan for WiFi access points and connect to one

export default class WifiConnect extends Component {

  constructor(props) {
    super(props);
    app = window.app;
    
    this.state = {
      networks: []
    };
  }

  joinNetwork(i) {
    var network = this.state.networks[i];

    console.log("Joining network:", network.ssid);
    
    alert("TODO not yet implemented");
  }
  
  rescan() {
    this.setState({
      networks: []
    });
    
    app.socket.send('f', 'wifi_scan', function(err) {
      if(err) return console.error(err); // TODO better error handling
    });
  }

  manualSetup() {
    alert("TODO Not yet implemented");
  }
  
  componentDidMount() {

    // TODO this is hacky. I should implement proper callbacks in the firmware
    // Hm did I already implement something like this for disaster.radio?
    app.socket.addListener('w', function(err, networks) {
      if(err) return console.error(err); // TODO better error handling

      networks = JSON.parse(networks.toString('utf8'));

      console.log(networks);
      
      this.setState({
        networks: networks
      });
      
    }.bind(this));

    this.rescan();
  }

  
  networkLink(i) {
    var net = this.state.networks[i];
    return (
      <li onclick={() => {this.joinNetwork(i)}}>
        <span class="name">{net.ssid}</span>
        <span class="secure">[{(net.secure > 0) ? 'secure' : 'open'}]</span>
      </li>
    );
  }

  // skip setup
  skip() {
    route('/');
  }
  
  render() {

    var list = [];
    var i;
    for(i=0; i < this.state.networks.length; i++) {
      list.push(this.networkLink(i));
    }

    var buttons = [];
    var status = '';
    if(this.state.networks.length < 1) {
      status = "Scanning for WiFi networks...";
    } else {
      status = "Click a network to join:";
      buttons.push((
        <button onclick={this.rescan}>Re-scan for WiFi networks</button>
      ));
      buttons.push((
        <button onclick={this.manualSetup}>Manual setup</button>
      ));
      buttons.push((
        <button onclick={this.skip}>Skip setup</button>
      ));
    }

    return (
      <div class="wifi-connect">
        <div class="status">{status}</div>
        <ul class="scan-results">
          {list}
        </ul>
        <div>
          {buttons}
        </div>
      </div>
    );
  }
}
