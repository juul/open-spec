
import { h, render, Component } from 'preact';
import Router from 'preact-router';
import { route } from 'preact-router';
import Plot from './Plot.js';
import Loading from './Loading.js';
import Main from './Main.js';
import WifiConnect from './WifiConnect.js';


export default class Root extends Component {

  constructor(props) {
    super();
    var app = window.app;
    
    this.state = {
      status: 'LOADING',
      interpolateMode: 'akima'
    }
    
  }
  
  componentDidMount() {
    app.socket.connect(function(err, isConnected, methods) {
      if(err) return console.error(err);    

      console.log("connected:", isConnected);

      if(!isConnected) {
        app.rpc = null;
      }
      
      if(!methods) return;
      console.log("Got RPC methods:", methods);

      app.rpc = methods;

      /*
      app.rpc.test_multi(function(err, resp) {
        if(err) return console.error(err);

        resp = JSON.parse(resp);
        console.log(resp);
      });
      */

      var large = {
        foo: 'boo',
        data: ""
      }
//      var i;
//      for(i=0; i < 17 * 1024; i++) {
//        large.data += '.';
//      }
      
      app.rpc.test_json(large, function(err, resp) {
        if(err) return console.error(err);

        resp = JSON.parse(resp);
        console.log(resp);
      });
      
/*
      
      app.socket.send('f', 'get_status', function(err, status) {
        if(err) return console.error(err);

        status = status.toString('utf8');
        if(status == 'UNCONFIGURED') {

          process.nextTick(function() {
            route('/setup', true);
          });
        }
        
        this.setState({
          status: status
        });
        
        // add listener for status change
        app.socket.addListener('s', function(err, status) {
          if(err) return console.error(err);

          this.setState({
            status: status
          });
          
        }.bind(this));
      }.bind(this));
*/
    }.bind(this));
  }

  render(props, state) {

    if(this.state.status == 'LOADING') {
      return (
        <Loading />
      );
    }
    
    return (
      <Router>
		    <Main path="/" />
        <WifiConnect path="/setup" />
	    </Router>
    );

  }
}


