
import { h, render, Component } from 'preact';
import Akima from 'akima-interpolator';

var app;

// Loading screen shown before connect

export default class Loading extends Component {

  constructor(props) {
    super(props);
    app = window.app;
  }

  render() {
    return (
      <div class="loading">
        <div class="loading-text">Loading...</div>
      </div>
    );
  }
}
