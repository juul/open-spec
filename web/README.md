
# Setup

```
npm install
cp settings.js.example settings.js
```

# Building and running

The first time you build you need to do:

```
npm run build-css
```

```
npm run build # build the js and css
npm start # run the simulator server
```

Then in a browser open http://localhost:8000/

# Developing

```
npm run dev # starts web server and auto-builder
```

# Uploading to ESP32

ToDo


