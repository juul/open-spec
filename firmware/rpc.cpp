
#include <ESPAsyncWebServer.h>
#include <ArduinoJson.h>
#include "rpc.h"

#define MAX_JSON_SIZE (8192) // maximum size of received json document

RPCList* list;

int rpc_list_init() {
  list = (RPCList*) malloc(sizeof(RPCList));
  if(!list) return -1;
  list->head = NULL;
  return 0;
}

RPCItem* rpc_list_add(AsyncWebSocketClient* client, RPCExport* exp, unsigned int rpc_id, void* req, size_t req_len) {
  RPCItem* cur = list->head;
  
  RPCItem* item = (RPCItem*) malloc(sizeof(RPCItem));
  if(!item) return NULL;
  
  item->client = client;
  item->exp = exp;
  item->id = rpc_id;
  item->req = req;
  item->req_len = req_len;
  item->state = NULL;
  item->next = NULL;
  
  if(!cur) {
    list->head = item;
  } else {
    while(cur) {
      if(!cur->next) {
        cur->next = item;
        break;
      }
      cur = cur->next;
    }
  }
  return item;
}

// retrieve the list item and optionally remove it from the list
RPCItem* rpc_list_get_by_id(uint32_t client_id, unsigned int id, int remove) {
  RPCItem* cur = list->head;
  RPCItem* prev = NULL;

  while(cur) {
    if(cur->id == id) {
      if(remove) {
        if(prev) {
          prev->next = cur->next;
        } else {
          list->head = cur->next;
        }
      }
      return cur;
    }
    prev = cur;
    cur = cur->next;
  }
  return NULL;
}

void rpc_item_free(RPCItem* item) {
  if(item->req) {
    if(item->exp->argument == 'j') {
      delete ((DynamicJsonDocument*) item->req);
    } else {
      free(item->req);
    }
  }
  if(item->state) {
    // TODO detect if item->state was allocated with the `new` keyword
    // and if so, unallocate with 
    free(item->state);
  }
  free(item);
}

// remove all items for a specific client from the list
// (used when a client disconnects)
// returns number of items removed
int rpc_list_del_all_by_client(AsyncWebSocketClient* client) {
  RPCItem* cur = list->head;
  RPCItem* prev = NULL;
  int count = 0;

  while(cur) {
    if(cur->client == client) {
      if(prev) {
        prev->next = cur->next;
      } else {
        list->head = cur->next;
      }
      rpc_item_free(cur);
      count++;
    }
    prev = cur;
    cur = cur->next;
  }
  return count;
}

// if err is NULL and resp_len is 0
// then it is assumed that resp is a DynamicJsonDocument
int rpc_response(AsyncWebSocketClient* client, unsigned int rpc_id, const char* err, void* resp, size_t resp_len) {
  char* out = NULL;
  size_t size;
  DynamicJsonDocument* json_resp;
  
  if(!err) {
    if(resp_len == 0) {
      json_resp = (DynamicJsonDocument*) resp;
      size = measureJson(*json_resp);
    } else {
      size = resp_len;
    }
  } else {
    size = strlen(err);
  }
  
  out = (char*) malloc(3 + size + 1);
  if(!out) return -1;
  
  out[0] = rpc_id & 0xFF;
  out[1] = (rpc_id >> 8) & 0xFF;
  out[2] = (err ? 1 : 0); 
  
  // TODO add "is more data coming" as bit 1 of metadata bitfield

  /*
     -----------------
     metadata bitfield
s     -----------------

     bit 0: 1 if this is an error string, 0 otherwise
     bit 1: 1 if more data is coming (for a stream), 0 otherwise
     bit 2-7: reserved for future use
  */
  
  if(!err) {
    if(resp_len == 0) {
      size = serializeJson(*json_resp, out + 3, size + 1);
    } else {
      memcpy(out + 3, resp, size);
    }
  } else {
    memcpy(out + 3, err, size);
  }
  
  // TODO check return value
  client->binary(out, size + 3);
  
  // It's safe to free this memory already even though it hasn't been sent yet
  // (since this is an async websocket server)
  // because the client->binary() call makes a copy of the data
  free(out);
  return 0;
}

int rpc_done(RPCItem* item, const char* err, void* resp, size_t resp_len) {
  int ret;

  rpc_list_get_by_id(item->client->id(), item->id, 1); // delete item from list
  
  if(item->exp->response == 'j') {
    ret = rpc_response(item->client, item->id, err, resp, 0);
  } else {
    ret = rpc_response(item->client, item->id, err, resp, resp_len);
  }
  
  // TODO error handling

  rpc_item_free(item);
  return ret;
}

int rpc_call(AsyncWebSocketClient* client, unsigned int rpc_id, char* rpc_name, char* data, size_t data_length) {
  int ret;
  DeserializationError err;
  const char* json_err;
  RPCItem* item;
  RPCExport* exp;
  DynamicJsonDocument* o;
  size_t to_alloc;
  
  if(rpc_list_get_by_id(client->id(), rpc_id, 1)) {
    // We already have a call with this ID
    // so ensure all calls with this ID are removed
    // and return an error
    while(rpc_list_get_by_id(client->id(), rpc_id, 1)) {};
    
    return rpc_response(client, rpc_id, "More than one RPC call in progress with the same ID. Call IDs must be unique", NULL, 0);
  }
  
  exp = rpc_get_export_by_name(rpc_name);
  if(!exp) {
    return rpc_response(client, rpc_id, "No exported method with that name exists", NULL, 0);
  }
  
  if(data_length) {
    // TODO remove debug
    char* d = (char*) malloc(data_length + 1);
    memcpy(d, data, data_length + 1);
    d[data_length] = '\0';
    Serial.printf("Got data of %u bytes: %s\r\n", data_length, d);
    free(d);

    if(exp->argument == 'j') {
      // we don't know how much memory we'll need
      // so first try to allocate 
      to_alloc = (size_t) (data_length * 1.5);
      while(1) {
        o = new (std::nothrow) DynamicJsonDocument(to_alloc);
        if(!o) {
          return rpc_response(client, rpc_id, "Not enough memory for JSON argument", NULL, 0);
        }
        err = deserializeJson(*o, data, data_length);
        if(err != DeserializationError::Ok) {
          if(err == DeserializationError::NoMemory) {
            // we didn't allocate enough memory to fit the the JSON
            // so de-allocate, increase allocation size and try again
            delete o;
            to_alloc = (size_t) (to_alloc * 1.5);
            Serial.printf("JSON allocation was too small. Trying again with %u bytes\r\n", to_alloc);
            if(to_alloc > MAX_JSON_SIZE) {
              return rpc_response(client, rpc_id, "Received JSON larger than allow maximum", NULL, 0);
            }
            continue;
          } else {
            json_err = err.c_str();
            ret = rpc_response(client, rpc_id, json_err, NULL, 0);
            delete o;
            return ret;
          }
        }
        break;
      }
      item = rpc_list_add(client, exp, rpc_id, o, 0);
      
      exp->func(o, 0, rpc_done, item);
    } else if(exp->argument == 'b') {
      item = rpc_list_add(client, exp, rpc_id, o, data_length);

      exp->func(data, data_length, rpc_done, item);
    } else {
      // TODO handle this situation
    }
  } else { // no argument
    item = rpc_list_add(client, exp, rpc_id, NULL, 0);
    exp->func(NULL, 0, rpc_done, item);
  }
  

  return 0;
}

void rpc_loop() {
  RPCItem* cur = list->head;

  while(cur) {
    if(cur->exp && cur->exp->func) {
      cur->exp->func(cur->req, cur->req_len, rpc_done, cur);
    }
    cur = cur->next;
  }
}

int rpc_init() {
  if(rpc_exports_init() < 0) {
    return -1;
  }
  return rpc_list_init();
}
