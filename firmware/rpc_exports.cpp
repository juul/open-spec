
#include "rpc.h"

// head of linked list of user-defined exported RPC methods
// format: {<function name>, <function pointer>, <argument type>, <response type>, <function type>}
// see the RPCExport declaration in rpc_exports.h
RPCExport* rpc_methods;

// Example of an RPC that responds with a JSON object
// TODO how to detect if o is NULL?
void rpc_test_json(void* arg, size_t arg_len, rpc_callback cb, RPCItem* item) {
  DynamicJsonDocument* req = (DynamicJsonDocument*) arg;
  DynamicJsonDocument* resp = new (std::nothrow) DynamicJsonDocument(200);
  if(!resp) {
    cb(item, "Not enough memory for JSON response", NULL, 0);
    return;
  }

  const char* foo = (*req)["foo"];

  Serial.printf("foo() called: %s\r\n", foo);
  
  (*resp)["foo"] = "cookie cat";

  cb(item, NULL, resp, 0);
  // you must free any memory you allocate after the call to cb()
  delete resp;
}

// Example of an RPC that calls back with an error
void rpc_test_error(void* arg, size_t arg_len, rpc_callback cb, RPCItem* item) {

  cb(item, "Error doing anything", NULL, 0);
}


void rpc_test_binary(void* arg, size_t arg_len, rpc_callback cb, RPCItem* item) {
  char str[] = "FOO";
  char* resp = (char*) malloc(arg_len);

  if(!resp) {
    cb(item, "Out of memory", NULL, 0);
    return;
  }

  if(arg_len >= 7) {
    memcpy(resp, arg, arg_len);
    memcpy(resp + 4, str, 3);
  }

  cb(item, NULL, resp, arg_len);
  free(resp);
}

// Example of an rpc function that doesn't immediately call its callback
// but waits until it's been called a total of three times.
// This is useful if the rpc function needs to wait for
// data that isn't yet available (e.g. from a sensor) before
// sending its response to the client
void rpc_test_multi(void* arg, size_t arg_len, rpc_callback cb, RPCItem* item) {
  char* i;
  
  if(!item->state) { // this must be the first time we've been called
    
    // item->state will be automatically unallocated with free()
    // so for now you cannot allocate it with the `new` keyword
    item->state = malloc(sizeof(char)); // allocate some memory for our state
    if(!item->state) {
      cb(item, "Not enough memory for state data", NULL, 0);
      return;
    }
    i = (char*) item->state;
    *i = 0; // initialize state to 0
  } else {
    i = (char*) item->state;
    (*i)++; // increment state;
  }

  if(*i >= 2) {
    DynamicJsonDocument* resp = new (std::nothrow) DynamicJsonDocument(200);
    if(!resp) {
      cb(item, "Not enough memory for JSON response", NULL, 0);
      return;
    }
    
    (*resp)["count"] = *i;

    cb(item, NULL, resp, 0);

    // you _must_ manually free any memory you've allocated
    delete resp;
    // apart from stuff in item-state.
    // free() will be called on item->state automatically
    // if item->state is not NULL
    return;
  }

  // if we aren't ready to call back then just return
  // without calling the callback
}

// add a new exported RPC method to the rpc_methods linked list
int rpc_export(char* name, rpc_function func, char argument, char response, char type) {
  RPCExport* cur;
  RPCExport* exp = (RPCExport*) malloc(sizeof(RPCExport));
  if(!exp) return -1;

  exp->name = name;
  exp->func = func;
  exp->argument = argument;
  exp->response = response;
  exp->type = type;
  exp->next = NULL;
  
  if(!rpc_methods) {
    rpc_methods = exp;
    return 0;
  }
  
  cur = rpc_methods;
  while(cur) {
    if(!cur->next) {
      cur->next = exp;
      break;
    }
    cur = cur->next;
  }
  
  return 0;
}

int rpc_unexport(char* name) {
  RPCExport* cur;
  RPCExport* prev = NULL;

  cur = rpc_methods;
  while(cur) {
    if(strcmp(cur->name, name) == 0) {
      if(cur == rpc_methods) {
        rpc_methods = NULL;
      } else {
        prev->next = cur->next;
      }
      free(cur);
      return 0;
    }
    prev = cur;
    cur = cur->next;
  }
  
  return -1;
}

RPCExport* rpc_get_export_by_name(char* name) {
  RPCExport* cur;

  cur = rpc_methods;
  while(cur) {
    if(strcmp(cur->name, name) == 0) {
      return cur;
    }
    cur = cur->next;
  }
  return NULL;
}

void rpc_list(void* arg, size_t arg_len, rpc_callback cb, RPCItem* item) {
  DynamicJsonDocument* resp;
  JsonArray root;
  JsonArray cur;
  RPCExport* c;
  
  // calculator for required data storage:
  // https://arduinojson.org/v6/assistant/
  int size = 0;

  c = rpc_methods;
  while(c) {
    // 64 is base size of data structure for each string
    // 3 is size of three empty strings
    // then add size of strings without trailing \0
    // then add size of two single-char strings
    size += 64 + 3 + strlen(c->name) + 2;
    c = c->next;
  }

  resp = new (std::nothrow) DynamicJsonDocument(size);
  if(!resp) {
    cb(item, "Not enough memory for JSON response", NULL, 0);
    return;
  }
  root = resp->to<JsonArray>();
  
  c = rpc_methods;
  while(c) {
    if(c->name[0] == '_') {
      c = c->next;
      continue;
    }
    cur = root.createNestedArray();
    cur.add(c->name);
    cur.add(c->argument);
    cur.add(c->response);
    cur.add(c->type);
    c = c->next;
  }
  
  cb(item, NULL, resp, 0);
}

int rpc_export_test_functions() {
  if(rpc_export("test_json", rpc_test_json, 'j', 'j', 'f') < 0) {
    return -1;
  }
  if(rpc_export("test_binary", rpc_test_binary, 'b', 'b', 'f') < 0) {
    return -1;
  }
  if(rpc_export("test_error", rpc_test_error, 'j', 'j', 'f') < 0) {
    return -1;
  }
  if(rpc_export("test_multi", rpc_test_multi, 'j', 'j', 'f') < 0) {
    return -1;
  }
  return 0;
}

int rpc_exports_init() {
  rpc_methods = NULL;
  if(rpc_export("_list", rpc_list, 'j', 'j', 'f') < 0) {
    return -1;
  }

  // comment this out if you don't want the test functions
  if(rpc_export_test_functions()) {
    return -1;
  }
  return 0;
}
