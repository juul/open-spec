//#define FS_NO_GLOBALS // Tell SPIFFS not to polute global namespace

#include <WiFi.h>
#include <ESPmDNS.h>
#include <FS.h>
#include <SPIFFS.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <ArduinoJson.h>
#include "rpc.h"

//#include "esp_idf_wifi.h"

#define MSG_SIZE_MAX (64 * 1024) // maximum allowed incoming message size
#define WIFI_SSID_MIN (1)
#define WIFI_SSID_MAX (31)
#define WIFI_PASS_MIN (8)
#define WIFI_PASS_MAX (63)
#define WIFI_CHANNEL (11)
#define WIFI_CLIENTS_MAX (4)
#define HOSTNAME "openspec"

char* set_ssid();
int wifi_connect(char* ssid, char* pass);
int set_state(int new_state);
  
AsyncWebServer server(80);
AsyncWebSocket ws("/ws");
AsyncEventSource events("/events");

// The basename for the SSID. Part of the MAC address is added at the end.
#define WIFI_AP_SSID "OpenSpec"
char wifi_ssid[WIFI_SSID_MAX+1];

#define WIFI_CONFIG_FILE "/config.txt"


enum STATES {
  STATE_BOOT = 0, // first state before anything happens
  STATE_UNCONFIGURED, // AP is active
  STATE_CONNECTING, // trying to connect as client
  STATE_CONNECTED, // connected as client
  STATE_ERROR // an unrecoverable error occurred
};

const char* state_names[] = {
  "BOOT",
  "UNCONFIGURED",
  "CONNECTING",
  "CONNECTED",
  "ERROR"
};

// are we currently scanning for wifi networks?
int wifi_scanning = 0;

char* err_msg;
int state = STATE_BOOT;

void die(char* msg) {
  err_msg = msg;
  Serial.printf("Fatal error: %s\r\n", msg);
  set_state(STATE_ERROR);
}


char* get_state_name() {
  return (char*) state_names[state];
}


int wsSendMsgAll(int id, char ns, char* data) {
  // format: <2-char id><1-char namespace>|<msg>
  
  // TODO change to using String 
  int len = 2 + 1 + 1 + strlen(data);

  char* msg = (char*) malloc(len);
  if(!msg) return -1;

  snprintf(msg, 2, "%ud", id);
  
  msg[2] = ns;
  msg[3] = '|';
  memcpy(msg+4, data, len - 4);
  
  ws.binaryAll(msg, len);
  
  free(msg);
}

int set_state(int new_state) {
  state = new_state;
  return wsSendMsgAll(0, 's', (char*) state_names[new_state]);
}

// Attempt to read config from file.
// If it is present then attempt to connect
int wifi_init() {
  char b;
  char ssid[WIFI_SSID_MAX + 1];
  char pass[WIFI_PASS_MAX + 1];
  int got_ssid = 0;
  int i;
  File cf;
  
  if(!WiFi.mode(WIFI_AP_STA)) {
    die("Failed to set combined access point + station mode");
    return -1;
  }
  if(!WiFi.softAP(set_ssid(), NULL, WIFI_CHANNEL, 0, WIFI_CLIENTS_MAX)) {
    die("Failed to start access point");
    return -1;
  }

  if(!MDNS.begin(HOSTNAME)) {
    die("Failed to start mDNS service");
    return -1;
  }
  //MDNS.addService("http", "tcp", 80);
  
  set_state(STATE_UNCONFIGURED);

  cf = SPIFFS.open(WIFI_CONFIG_FILE, "r");
  if(!cf) {
    die("Unable to open or create config.txt (wifi config file)");
    return 0;
  }
  if(cf.size() < WIFI_SSID_MIN + 1 + WIFI_PASS_MIN) {
    cf.close();
    return 0;
  }

  i = 0;
  while(b = cf.read() != -1) {
    if(b == '\r') continue;
    if(b == '\n') {
      if(!got_ssid) {
        got_ssid = 1;
        ssid[i] = '\0';
        i = 0;
      } else {
        pass[i] = '\0';
        break;
      }
    }
    if(!got_ssid) {
      ssid[i] = b;
    } else {
      pass[i] = b;
    }
    i++;
  }

  if(b == -1) {
    cf.close();
    return 0;
  }

  return wifi_connect(ssid, pass);
}

int wifi_config_delete() {
  if(!SPIFFS.remove(WIFI_CONFIG_FILE)) {
    return -1;
  }
}

int wifi_connect(char* ssid, char* pass) {
  if(!WiFi.begin(ssid, pass)) {
    return -1;
  }
  return set_state(STATE_CONNECTING);
}

int wifi_config_write(char* ssid, char* pass) {
  File cf;
  cf = SPIFFS.open(WIFI_CONFIG_FILE, "w");
  if(!cf) {
    goto fail;
  }
  if(!cf.print(ssid)) goto fail;
  if(!cf.print("\n")) goto fail;  
  if(!cf.print(pass)) goto fail;  
  if(!cf.print("\n")) goto fail;
  
  cf.close();
  return 0;
  
 fail:
   die("Unable to save WiFi configuration");
   cf.close();
   return -1;
}


void wifi_scan_complete(int networksFound) {
  wifi_scanning = 0;
  // format: <2-char id><1-char namespace>|<msg>  
  String json = "00w|";

  json += "[";

  int i;
  for (i=0; i < networksFound; i++){
    if(i) json += ",";
    json += "{";
    json += "\"rssi\":"+String(WiFi.RSSI(i));
    json += ",\"ssid\":\""+WiFi.SSID(i)+"\"";
    json += ",\"bssid\":\""+WiFi.BSSIDstr(i)+"\"";
    json += ",\"channel\":"+String(WiFi.channel(i));
    json += ",\"secure\":"+String(WiFi.encryptionType(i));
    json += "}";
  }
  json += "]";

  ws.binaryAll(json);
  
  json = String();
  WiFi.scanDelete();
}

// Unfortunately WiFi.scanNetworksAsync is only available on ESP8266, not ESP32
// so we have to call WiFi.scanNetworks() with the async flag
// and then poll WiFi.scanComplete() in the main loop
int wifi_scan() {
  if(wifi_scanning) return 0; // don't initiate a scan if one is already underway
  
  if(WiFi.scanNetworks(true, false) != WIFI_SCAN_RUNNING) {
    Serial.printf("Failed to start wifi scan\r\n");
    return -1;
  }
  wifi_scanning = 1;
  return 0;
}

typedef struct sFrameBuffer{
  uint32_t client_id; // the client associated with this buffer
  uint32_t msg_id; // the message associated with this buffer
  char* data; // buffer data
  size_t written; // number of bytes written so far
  size_t size; // total allocated buffer size
  struct sFrameBuffer* next;
} FrameBuffer;

FrameBuffer* fb_head = NULL; // head of linked list of FrameBuffers

FrameBuffer* get_frame_buffer(uint32_t client_id, uint32_t msg_id) {
  FrameBuffer* cur;
  cur = fb_head;
  while(cur) {
    if(cur->client_id == client_id && cur->msg_id == msg_id) {
      return cur;
    }
    cur = cur->next;
  }
  return NULL;
}

// if `all` then remove all frame buffers for this client
void remove_frame_buffer(uint32_t client_id, uint32_t msg_id, int all) {
  FrameBuffer* prev = NULL;
  FrameBuffer* cur;
  
  cur = fb_head;
  while(cur) {
    if(cur->client_id == client_id && (all || cur->msg_id == msg_id)) {
      if(cur == fb_head) {
        fb_head = NULL;
      } else {
        prev->next = cur->next;
      }
      if(cur->data) {
        free(cur->data);
      }
      free(cur);
      break;
    }
    prev = cur;
    cur = cur->next;
  }
}

FrameBuffer* create_frame_buffer(uint32_t client_id, uint32_t msg_id, size_t size) {
  FrameBuffer* cur;
  FrameBuffer* fb;
  
  if(size > MSG_SIZE_MAX) {
    return NULL;
  }
  
  // ensure there is only ever one frame buffer per client+msg combo
  remove_frame_buffer(client_id, msg_id, 0);
  
  fb = (FrameBuffer*) malloc(sizeof(FrameBuffer));
  if(!fb) return NULL;
  
  fb->client_id = client_id;
  fb->msg_id = msg_id;
  
  fb->data = (char*) malloc(size);
  if(!fb->data) return NULL;
  
  memset(fb->data, 0, size);
  
  fb->size = size;
  fb->written = 0;
  fb->next = NULL;
  
  if(!fb_head) {
    fb_head = fb;
  } else {
    cur = fb_head;
    while(cur) {
      if(!cur->next) {
        cur->next = fb;
        break;
      }
      cur = cur->next;
    }
  }
  return fb;
}

int write_frame_buffer(FrameBuffer* fb, size_t offset, char* data, size_t len) {
  size_t new_size;

  // multi-frame messages all have index == 0 (index is the same as offset)
  // so we have to keep track of how much was written ourselves
  if(offset == 0 && fb->written > 0) {
    offset = fb->written;
  }
  new_size = offset + len;
  
  // TODO check if _total_ bytes of all allocated frame buffers exceeds a limit
  if(new_size > fb->size) {
    fb->data = (char*) realloc(fb->data, new_size);
    if(!fb->data) return -1;
    memset(fb->data + fb->size, 0, new_size - fb->size);
    fb->size = new_size;
  }
  
  memcpy(fb->data + offset, data, len);
  fb->written += len;
  return 0;
}

void send_error(AsyncWebSocketClient* client, const char* err) {
  size_t len;
  char* buf;

  Serial.printf("Sending error: %s\r\n", err);
  
  len = strlen(err);
  buf = (char*) malloc(2 + len); 
  if(!buf) return;
  
  // header is always 0x00 0x01 for errors
  buf[0] = 0x01;
  buf[1] = 0x00;

  memcpy(buf + 2, err, len);

  client->binary(buf, len + 2);
}

int handle_received_message(AsyncWebSocketClient* client, char* data, size_t len) {
  unsigned int rpc_call_id;
  size_t rpc_name_length;
  char* rpc_name;
  int data_length;

  // debug output
  Serial.printf("ws[%u] message: ", client->id());
  for(size_t i=0; i < len; i++) {
    Serial.printf("%02x ", data[i]);
  }
  Serial.printf("\r\n");
      
  // first two bytes make up the call ID
  rpc_call_id = (data[1] << 8) | data[0];
  // third byte is the length of name of the rpc function called
  rpc_name_length = data[2];
  
  Serial.printf("RPC name length: %u\r\n", rpc_name_length);
      
  if(rpc_name_length == 0 || rpc_name_length + 3 > len) {
    send_error(client, "Invalid RPC name length");
    return -1;
  }
  
  rpc_name = (char*) malloc(rpc_name_length + 1);
  if(!rpc_name) {
    send_error(client, "Out of memory");
    return -1;
  }
  memcpy(rpc_name, data + 3, rpc_name_length);
  rpc_name[rpc_name_length] = '\0';
  
  Serial.printf("RPC name: %s\r\n", rpc_name);
      
  data += 3 + rpc_name_length;
  data_length = len - 3 - rpc_name_length;

  Serial.printf("Remaining data length: %u\r\n", data_length);
      
  Serial.printf("Got call with ID %u and name %s\r\n", rpc_call_id, rpc_name);
      
  // the rest of the data is the argument for the rpc call (if any)
  if(data_length <= 0) {
    // TODO error handling
    rpc_call(client, rpc_call_id, rpc_name, NULL, 0);
  } else {
    rpc_call(client, rpc_call_id, rpc_name, (char*) data, data_length);
  }
  
  free(rpc_name);
  
  return 0;
}

/*
Fragmentation and chunking
--------------------------
Messages can be both fragmented and chunked. 
For more info see `docs/websocket_fragments_and_chunks.md`

TODO:

Maybe let the RPC functions decide for themselves if they want the chunks/fragments
as they come in, or if they want them as one large buffer?
This could be an option in the rpc_methods[] array of exported functions.
*/
void onWSEvent(AsyncWebSocket* server, AsyncWebSocketClient* client, AwsEventType type, void* arg, uint8_t* data, size_t len) {
  if(type == WS_EVT_CONNECT){
    
    //client connected
    Serial.printf("ws[%s][%u] connect\r\n", server->url(), client->id());
    
    client->ping();
    
  } else if(type == WS_EVT_DISCONNECT){
    //client disconnected
    Serial.printf("ws[%s][%u] disconnect: %u\r\n", server->url(), client->id());

    // ensure frame buffer for this client is removed (if any)
    remove_frame_buffer(client->id(), 0, 1);
    
  } else if(type == WS_EVT_ERROR){
    //error was received from the other end
    Serial.printf("ws[%s][%u] error(%u): %s\r\n", server->url(), client->id(), *((uint16_t*)arg), (char*)data);
    
  } else if(type == WS_EVT_PONG){
    
    //pong message was received (in response to a ping request maybe)
    Serial.printf("ws[%s][%u] pong[%u]: %s\r\n", server->url(), client->id(), len, (len)?(char*)data:"");
  } else if(type == WS_EVT_DATA){ // data frame received

    int ret;
    AwsFrameInfo * info = (AwsFrameInfo*)arg;


    Serial.printf("Frame final: %u\r\n", info->final);
    Serial.printf("Frame number: %u\r\n", info->num);
    Serial.printf("Frame index: %u\r\n", info->index);
    Serial.printf("Frame length: %u\r\n", info->len);
    Serial.printf("Chunk length: %u\r\n", len);
    Serial.printf("Opcode: %u\r\n", info->opcode);

    
    // this is part of a multi-frame and/or multi-chunk message?
    if((info->final && info->opcode == 0) || !info->final // multi-frame msg
       || info->index > 0 || info->len != len) { // multi-chunk msg

      //      Serial.printf("  multi-part!\r\n");
      
      FrameBuffer* fb;
      if((info->index == 0 && info->len != len) // first chunk in multi-chunk msg
         || (!info->final && info->opcode > 0)) { // first frame in multi-frame msg

        //        Serial.printf("  first part of multi-part!\r\n");
        
        fb = create_frame_buffer(client->id(), info->num, info->len);
        if(!fb) {
          send_error(client, "Out of memory");
          return;
        }
      } else {
        //        Serial.printf("  not first part of multi-part!\r\n");
        
        fb = get_frame_buffer(client->id(), info->num);
        if(!fb) {
          send_error(client, "Malformed frame received");
          return;
        }
      }
      if(write_frame_buffer(fb, info->index, (char*) data, len) < 0) {
        send_error(client, "Out of memory");
        return;
      }

      // last chunk or frame of a multi-chunk and/or multi-frame message
      if(info->final && (info->index + len >= info->len)) {
        //        Serial.printf("  last part of multi-part!\r\n");
        
        handle_received_message(client, fb->data, fb->size);
        remove_frame_buffer(fb->client_id, fb->msg_id, 0);
        return;
      }      

    } else { // neither a multi-chunk nor multi-frame message
      //      Serial.printf("  single-part message!\r\n");
      
      handle_received_message(client, (char*) data, len);
      
    }
  }
}

void webServerSetup() {
  ws.onEvent(onWSEvent);
  server.addHandler(&ws);
  
  //  events.onConnect([](AsyncEventSourceClient *client) {
  //    client->send("hello!", NULL, millis(), 1000);
  //  });

  server.addHandler(&events);

  server.serveStatic("/", SPIFFS, "/").setDefaultFile("index.htm");

  server.onNotFound([](AsyncWebServerRequest *request) {
    Serial.printf("NOT FOUND: http://%s%s\r\n", request->host().c_str(), request->url().c_str());
    request->send(404);
  });

  server.begin();
}

// intialize the ssid with by adding the last 5 chars from the MAC address
char* set_ssid() {
  const char* mac = WiFi.macAddress().c_str();

  snprintf(wifi_ssid, sizeof(WIFI_AP_SSID) + 1 + 5, "%s %s", WIFI_AP_SSID, mac + 12);

  return wifi_ssid;
}

// set the SSID to the SSID basename followed by a space,
// followed by the current IP assigned to this device by the DHCP server
int set_ssid_from_ip() {
  if(WiFi.status() != WL_CONNECTED) {
    return -1;
  }
  
  snprintf(wifi_ssid, sizeof(WIFI_AP_SSID) + 1 + 12 + 1, "%s %s", WIFI_AP_SSID, WiFi.localIP());

  if(!WiFi.softAP(wifi_ssid, NULL, WIFI_CHANNEL, 0, WIFI_CLIENTS_MAX)) {
    return -1;
  }
  
  return 0;
}

// switch from AP_STA to STA
int wifi_disable_ap() {

  // switch mode to station only
  if(!WiFi.mode(WIFI_STA)) {
    return -1;
  }

  // switch off AP (may not actually be necessary)
  if(!WiFi.softAPdisconnect(true)) {
    return -1;
  }

  return 0;
}


void setup() {
  Serial.begin(115200);
  Serial.printf("Init\n");
  SPIFFS.begin(); // TODO check success
  if(rpc_init() < 0) {
    Serial.printf("RPC init failed\r\n");
    return;
  }
  wifi_init(); // TODO check success


  webServerSetup(); // TODO check success
}

void loop() {
  int ret;
  if(state == STATE_CONNECTING) {
    if(WiFi.status() == WL_CONNECTED) {
      state == STATE_CONNECTED;

    }
  }
  if(wifi_scanning) {
    ret = WiFi.scanComplete();
    if(ret > -1) {
      printf("Scan complete!\r\n");
      wifi_scan_complete(ret);
    }
  }
  delay(100);
  rpc_loop();
  // TODO if disconnected for more than e.g. 30 seconds then re-enable access point
}
