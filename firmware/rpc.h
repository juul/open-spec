#include <ESPAsyncWebServer.h>
#include <ArduinoJson.h>

struct sRPCItem;

// function pointer type for RPC callbacks
typedef int (*rpc_callback)(struct sRPCItem* item, const char*, void*, size_t);
// function pointer type for RPC functions
typedef void (*rpc_function)(void*, size_t arg_len, rpc_callback, struct sRPCItem*);

// an exported RPC method
typedef struct sRPCExport {
  char* name;
  rpc_function func;
  char argument; // 'j' for JSON or 'b' for binary (also used for text)
  char response; // same types as argument
  char type; // 'f' for normal function, 'r' for readable stream or 'w' for writable string
  struct sRPCExport* next;
} RPCExport;

// an RPC in progress
typedef struct sRPCItem {
  AsyncWebSocketClient* client;
  RPCExport* exp; // the rpc export
  void* req; // the data sent from the RPC client
  size_t req_len; // length of data sent from the RPC client (or 0 if JsonDocument)
  unsigned int id; // unique ID of this RPC call
  void* state; // used by RPC function to keep state between calls
  struct sRPCItem* next;
} RPCItem;


typedef struct {
  RPCItem* head;  
} RPCList;


RPCExport* rpc_get_export_by_name(char* name);
int rpc_exports_init();
int rpc_list_init();

RPCItem* rpc_list_add(AsyncWebSocketClient* client, RPCExport* exp, unsigned int rpc_id, void*, size_t);

RPCItem* rpc_list_get_by_id(uint32_t client_id, unsigned int id, int remove);

int rpc_list_del_all_by_client(AsyncWebSocketClient* client);

int rpc_call(AsyncWebSocketClient* client, unsigned int rpc_id, char* rpc_name, char* data, size_t data_length);

int rpc_response(AsyncWebSocketClient* client, unsigned int rpc_id, const char* err, DynamicJsonDocument* resp);

int rpc_done(RPCItem* item, const char* err, void* resp, size_t resp_len);

void rpc_item_free(RPCItem* item);

int rpc_export(char* name, rpc_function func, char argument, char response, char type);

int rpc_unexport(char* name);

void rpc_loop();

int rpc_init();

